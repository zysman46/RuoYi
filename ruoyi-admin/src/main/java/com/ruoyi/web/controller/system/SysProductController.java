package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysProduct;
import com.ruoyi.system.service.ISysProductService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 产品Controller
 * 
 * @author ruoyi
 * @date 2022-02-28
 */
@Controller
@RequestMapping("/system/product")
public class SysProductController extends BaseController
{
    private String prefix = "system/product";

    @Autowired
    private ISysProductService sysProductService;

    @RequiresPermissions("system:product:view")
    @GetMapping()
    public String product()
    {
        return prefix + "/product";
    }

    /**
     * 查询产品树列表
     */
    @RequiresPermissions("system:product:list")
    @PostMapping("/list")
    @ResponseBody
    public List<SysProduct> list(SysProduct sysProduct)
    {
        List<SysProduct> list = sysProductService.selectSysProductList(sysProduct);
        return list;
    }

    /**
     * 导出产品列表
     */
    @RequiresPermissions("system:product:export")
    @Log(title = "产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysProduct sysProduct)
    {
        List<SysProduct> list = sysProductService.selectSysProductList(sysProduct);
        ExcelUtil<SysProduct> util = new ExcelUtil<>(SysProduct.class);
        return util.exportExcel(list, "产品数据");
    }

    /**
     * 新增产品
     */
    @GetMapping(value = { "/add/{productId}", "/add/" })
    public String add(@PathVariable(value = "productId", required = false) Long productId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(productId))
        {
            mmap.put("sysProduct", sysProductService.selectSysProductByProductId(productId));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存产品
     */
    @RequiresPermissions("system:product:add")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysProduct sysProduct)
    {
        return toAjax(sysProductService.insertSysProduct(sysProduct));
    }

    /**
     * 修改产品
     */
    @RequiresPermissions("system:product:edit")
    @GetMapping("/edit/{productId}")
    public String edit(@PathVariable("productId") Long productId, ModelMap mmap)
    {
        SysProduct sysProduct = sysProductService.selectSysProductByProductId(productId);
        mmap.put("sysProduct", sysProduct);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品
     */
    @RequiresPermissions("system:product:edit")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysProduct sysProduct)
    {
        return toAjax(sysProductService.updateSysProduct(sysProduct));
    }

    /**
     * 删除
     */
    @RequiresPermissions("system:product:remove")
    @Log(title = "产品", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{productId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("productId") Long productId)
    {
        return toAjax(sysProductService.deleteSysProductByProductId(productId));
    }

    /**
     * 选择产品树
     */
    @GetMapping(value = { "/selectProductTree/{productId}", "/selectProductTree/" })
    public String selectProductTree(@PathVariable(value = "productId", required = false) Long productId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(productId))
        {
            mmap.put("sysProduct", sysProductService.selectSysProductByProductId(productId));
        }
        return prefix + "/tree";
    }

    /**
     * 加载产品树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = sysProductService.selectSysProductTree();
        return ztrees;
    }
}
